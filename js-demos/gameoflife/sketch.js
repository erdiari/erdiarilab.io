let SizeX = 50
let SizeY = 50
let csizeX = 800 // canvas size
let csizeY = 800 // canvas size
const grid_size = csizeX / SizeX;
var is_paused = false //is_paused
let fr  //framerate
let game

function setup() {
    cvn = createCanvas(csizeX, csizeY);
    cvn.mousePressed(celledit);
    // UI
    fr = createSlider(1,30,1);
    fr.position(0.1 * csizeX,csizeY+10);
    pbut = createButton("Play/Pause");
    pbut.position(0.6 * csizeY,csizeY+10);
    pbut.mousePressed(dpause);
    // gamestuff
    game = new Game(SizeX,SizeY);
}

function draw() {
    background(0);
    fill(120);
    game.draw(grid_size);
    if (!is_paused){
        frameRate(fr.value());
        game.next();
    }
}

function dpause(){
    is_paused = !is_paused;
}

function celledit(){
    const x = Math.floor(mouseX  / grid_size);
    const y = Math.floor(mouseY / grid_size);
    game.change_cell(x,y);
}
