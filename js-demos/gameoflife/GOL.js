class Game{
    constructor(w,h){
        this.w = w;
        this.h = h;
        this.grid = new Array(w);
        for (var i=0; i<w; i++) {
            this.grid[i] = new Array(h);
        }
    }
    print(){
        print(this.grid);
    }
    next(){
        var cell_list = [];
        for (var i=0;i<this.w;i++) {
            for (var j=0;j<this.h;j++) {
                if (this.grid[i][j]){
                    cell_list.push([i,j]);
                }
            }
        }
        var change = new Array(this.w);
        for (var i=0; i<this.w; i++) {
            change[i] = new Array(this.h).fill(0);
        }
        cell_list.forEach(cell => {
            const cx = cell[0];
            const cy = cell[1];
            for (var i=-1;i<=1;i++) {
                for (var j=-1;j<=1;j++) {
                    const cordx = cx + i;
                    const cordy = cy + j;
                    if (this.is_in_grid(cordx,cordy) && !(i == 0 && j ==0))
                    {change[cordx][cordy]+= 1;}
                }
            }
        });
        for (var i=0;i<this.w;i++) {
            for (var j=0;j<this.h;j++) {
                if (!(this.grid[i][j]) && change[i][j] ==3 ) {this.add_cell(i,j)}
            }
        };
        cell_list.forEach(cell=> {
            const x = cell[0];
            const y = cell[1];
            if (!(change[x][y] >= 2 && change[x][y] <=3)) {this.rm_cell(x,y);}
        });
        // var table = "<table>";
        // change.forEach(row =>{
        //     table += "<tr>";
        //     row.forEach(cell =>{
        //         table += "<td>" + cell + "</td>";
        //     });
        //     table += "</tr>";
        // });
        // table += "</table>";
        // document.getElementById("ch_tab").innerHTML = table;
    }
    is_in_grid(x,y){
        return (x >= 0 && y >= 0 && x<this.w && y<this.h);
    }
    change_cell(x,y){
        if (!(this.is_in_grid(x,y))) {return false;};
        this.grid[x][y] = !this.grid[x][y];
        return true;
    }
    add_cell(x,y){
        if (!(this.is_in_grid(x,y))) {return false;};
        this.grid[x][y] = true;
        return true;
    }
    rm_cell(x,y){
        if (!(this.is_in_grid(x,y))) {return false;};
        this.grid[x][y] = false;
        return true;
    }
    draw(grid_size){
        for(var i=0; i<this.w; i++){
            for (var j=0;j<this.h;j++) {
                if (this.grid[i][j]){
                    square(i* grid_size, j* grid_size, grid_size);
                }
            }
        }
    }
}
