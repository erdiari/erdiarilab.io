function setup() {
    player = new Player(200,200,120,100);
    bmap = new map(400,400);
    createCanvas(bmap.w, bmap.h);
    for (let i = 0; i < 5; i++) {
        bmap.add_line_object(random(bmap.w),random(bmap.h),random(bmap.w),random(bmap.h));
    }
}

function draw() {
    background(0);
    if (mouseIsPressed) {
        if (mouseButton === LEFT) {
            player.change_pos(mouseX,mouseY);
        }
    }
    bmap.show(player);
}

function mouseWheel(event){
    player.change_look_dir(event.delta);
    return false;
}
