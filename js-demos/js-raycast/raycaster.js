class Player{
    constructor(x,y,fov,max_look) {
        this.cord = createVector(x,y);
        this.max_look = max_look;
        this.fov = fov/2; // We always will use half of fov so we store that.
        this.look = createVector(1,0).mult(max_look); 
        this.look_a = this.look.heading();
        angleMode(DEGREES);
    }
    change_look_dir(angle){
        this.look = this.look.rotate(angle);
        this.look_a = this.look.heading();
    }
    can_see_dot(x,y){
        var r_pos = createVector(x - this.cord.x, y - this.cord.y);
        // stroke(255,255,255,100);
        // line(this.cord.x,this.cord.y,this.cord.x+r_pos.x,this.cord.y+r_pos.y);
        var r_pos_mag = r_pos.mag();
        if (r_pos_mag < this.max_look){
            // use dot product to find angle between  vectors
            // and return whether it is inside fov or not.
            // let theta = abs(acos(this.look.dot(r_pos)/(r_pos_mag*this.max_look)));
            let theta = abs(this.look.angleBetween(r_pos));
            return (theta < this.fov);
        } 
        else { return  false};
    }
    ray_cast(r_x,r_y,x1,y1,x2,y2){
        // This function can be optimized.
        var a = createVector(x1,y1);
        var b = createVector(x2,y2);
        var v1 = this.cord.copy().sub(a);
        var v2 = b.copy().sub(a);
        var v3 = createVector(-r_y,r_x); //ray rotated 90 degrees.
        var t2 = v1.dot(v3) / v2.dot(v3);
        if (t2>0 && t2<1){
            return v2.cross(v1).div(v2.dot(v3)).mag(); //lenght of ray.
        } else {return false;}
    }
    change_pos(x,y){
        this.cord.x =x;
        this.cord.y=y;
    }
    show(){
        fill(255);
        circle(this.cord.x,this.cord.y,20);
        fill(255,255,255,100);
        angleMode(DEGREES);
        arc(this.cord.x,this.cord.y,this.max_look*2,this.max_look*2,this.look_a-this.fov,this.look_a+this.fov);
        // stroke(255,0,0,255);
        // line(this.cord.x,this.cord.y,this.cord.x + this.look.x, this.cord.y+this.look.y);
    }
}

class p_dot{
    constructor(x,y){
        this.cord = createVector(x,y);
    }
    show(){
        fill(255,255,255,100);
        circle(this.cord.x,this.cord.y,10)
    }
}

class p_line{
    constructor(x1,y1,x2,y2){
        this.a = createVector(x1,y1);
        this.b = createVector(x2,y2);
        // this.cord = this.a.copy().add(this.b).div(2);
    }
    show(){
        p_line(this.a.x,this.a.y,this.b.x,this.b.y);
    }
}

class map{
    constructor(w, h){
        this.w = w;
        this.h = h;
        this.dot_objects = [];
        this.line_objects = [];
    }
    add_dot_object(x,y){ this.dot_objects.push(new p_dot(x,y)); }
    add_line_object(x1,y1,x2,y2){ this.line_objects.push(new p_line(x1,y1,x2,y2)); }
    player_seen_dot_objects(player){
        var sees = [];
        this.dot_objects.forEach(object => {
            if (player.can_see_dot(object.cord.x,object.cord.y)){
                sees.push(object);
            }
        });
        return sees;
    }_
    calculate_seen_line_segments(player){
        var inside = []; // lines which are inside view arc.
        var half_outside = []; // lines which their end points are outside of the view arc.
        line_objects.forEach(l_obj => {
            var is_a_inside = player.can_see_dot(l_obj.a.x,l_obj.a.y);
            var is_b_inside = player.can_see_dot(l_obj.b.x,l_obj.b.y);
            var lower_bound;
            var upper_bound;
            if (is_a_inside && is_b_inside){
                inside.push((is_b_inside,l_obj));
            } else if (is_a_inside){
                player.ray_cast(player.lower_bound.x,player.lower_bound.y,l_obj.a.x,l_obj.a.y,l_obj.b.xl_obj.b.x);
                player.ray_cast(player.upper_bound.x,player.upper_bound.y,l_obj.a.x,l_obj.a.y,l_obj.b.xl_obj.b.x);

            } else if (is_b_inside){

            }
        });
    }
    // player_seen_line_objects(player){
    //     var sees =[];
    //     this.line_objects.forEach(object => {
    //         if (player.can_see_line(object.a.x,object.a.y,object.b.x,object.b.y)){
    //             sees.push(object);
    //         }
    //     });
    //     return sees;
    // }
    show(player){
        stroke(255,255,255,100)
        fill(255,255,255,255);
        var d_objs = this.player_seen_dot_objects(player);
        d_objs.forEach(obj => {
            obj.show();
        });
        var l_objs = this.player_seen_line_objects(player);
        l_objs.forEach(obj => {
            obj.show();
        });
        player.show();
    }
}
